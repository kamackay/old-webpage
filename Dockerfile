FROM alpine:latest as builder

WORKDIR /app

RUN apk upgrade --update --no-cache && \
    apk add --no-cache \
        openjdk8 \
        maven

ADD pom.xml ./

RUN mvn dependency:go-offline

ADD . .

RUN mvn clean install

FROM tomcat:jdk8-openjdk-slim

COPY --from=builder /app/target/_-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war
